// Graf_G(n,p).cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdlib.h"
#include "iostream"
#include "time.h"
#include "omp.h"


int **create_matrix(int rows, int columns)
{
	int **matrix = new int*[rows];
	for (int i = 0; i < rows; i++)
		matrix[i] = new int[columns];
	return matrix;
}
void show_matrix(int **matrix, int rows, int columns)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			printf("%d\t", matrix[i][j]);
		}
		printf("\n");
	}
}
void destroy_matrix(int **&matrix, int rows, int columns)
{
	for (int i = 0; i < rows; ++i)
		delete[] matrix[i];

	delete[] matrix;
}
void gnp(double &start, double &end,int n, float p, int **&matrix)
{
	float x;
	int i, j;
	// zerowanie tablicy
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			matrix[i][j] = 0;
	}
	//Wpisywanie jedynki w wylosowane 'krawedzie'
	start = omp_get_wtime();
	#pragma omp parallel for shared(matrix,n) private(i,j)
	for (i = 0; i < n - 1; i++)
	{
		j = i + 1;
		for (j = j; j < n; j++)
		{
			x = rand() % 101;
			x = x / 100;
			if (x <= p)
			{
				matrix[i][j] = 1;
				matrix[j][i] = 1;
			}
		}
	}
	end = omp_get_wtime();
}
int _tmain(int argc, _TCHAR* argv[])
{
	int rows = 10, columns = 10;
	double start, end, result;
	//p - prawdopodobienstwo krawedziowe (0 <= p <= 1)
	//okreslona dla kazdej krawedzi, okresla ono prawdzopodobienstwo, ze dana krawedz znajdzie sie w grafie
	int **matrix = create_matrix(rows, columns);
	gnp(start,end,rows,0.9,matrix);
	show_matrix(matrix,rows,rows);
	result = end - start;
	std::cout << std::endl << "Czas wykonania w sekundach " << result << std::endl;
	destroy_matrix(matrix, rows, rows);
	return 0;
}

